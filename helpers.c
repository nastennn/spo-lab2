#include <stdio.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>

int error(const char *msg)
{
    printf("Error: %s\n", msg);
    return -1;
}

int parse(const char *cmd, char **args)
{
    const char *p = cmd;
    int count = 0;

    for (;;) {
        while (isspace(*p)) p++;
        if (count >= 3) {
            return count;
        }
        if (*p == '\0') break;

        if (*p == '"' || *p == '\'') {
            int quote = *p++;
            const char *begin = p;

            while (*p && *p != quote) p++;
            if (*p == '\0') return error("Unmatched quote");
            strncpy(args[count], begin, p-begin);
            count++;
            p++;
            continue;
        }

        if (strchr("<>()|", *p)) {
            args[count] = calloc(1, 256);
            strncpy(args[count], p, 1);
            count++;
            p++;
            continue;
        }

        if (isalnum(*p) || *p == '.' || *p == '/') {
            const char *begin = p;

            while (isalnum(*p) || *p == '.' || *p == '/') p++;
            strncpy(args[count], begin, p-begin);
            count++;
            continue;
        }

        return error("Illegal character");
    }

    return count;
}

int check_directory(const char *path) {
    DIR *dir = opendir(path);
    if (dir) return 1;
    else return 0;
}


void append_path_part(char *path, const char *part) {
    strcat(path, "/");
    strcat(path, part);
}







