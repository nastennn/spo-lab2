#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "fat32_structs.h"

unsigned int get_fat_table_value(unsigned int active_cluster, unsigned int first_fat_sector, unsigned int sector_size, int fd) {
    unsigned char *FAT_table = malloc(sector_size);
    unsigned int fat_offset = active_cluster * 4;
    unsigned int fat_sector = first_fat_sector + (fat_offset / sector_size);
    unsigned int ent_offset = fat_offset % sector_size;
    pread(fd, FAT_table, sector_size, fat_sector * sector_size);
    unsigned int table_value = *(unsigned int *) &FAT_table[ent_offset] & 0x0FFFFFFF;
    free(FAT_table);
    return table_value;
}

unsigned int get_first_sector(struct partition_value *part, unsigned int cluster) {
    return ((cluster - 2) * part->fat_boot->BPB_SecPerClus) + part->first_data_sector;
}

unsigned int read_file_cluster(struct partition_value *part, unsigned int cluster, char *buf) {
    unsigned int first_sector = get_first_sector(part, cluster);
    pread(part->device_fd, buf, part->cluster_size, first_sector * part->fat_boot->BPB_BytsPerSec);
    return get_fat_table_value(cluster, part->fat_boot->BPB_RsvdSecCnt, part->fat_boot->BPB_BytsPerSec,
                               part->device_fd);
}

struct file *init_file(struct dir_entry *entry, unsigned char *filename) {
    struct file *file = calloc(1, sizeof(struct file));
    file->filename = calloc(1, 256);
    strcpy((char *) file->filename, (char *) filename);
    file->size = entry->DIR_FileSize;
    file->type = ((entry->DIR_Attr & 0x20) == 0x20) ? 'f' : 'd';
    file->first_cluster = entry->DIR_FstClusHI << 4;
    file->first_cluster =
            file->first_cluster + (entry->DIR_FstClusLO & 0xFFFF);
    if (file->first_cluster == 0)
        file->first_cluster = 2;

    return file;
}

void free_file(struct file *file) {
    if (file) {
        free(file->filename);
        struct file *next = file;
        while (next != NULL) {
            struct file *current = next;
            next = current->next;
            free(current);
        }
    }
}

struct file *read_dir(struct partition_value *value, unsigned int first_cluster) {
    unsigned int cluster_size = value->cluster_size;
    unsigned int sector_size = value->fat_boot->BPB_BytsPerSec;
    unsigned int current_cluster = first_cluster;
    int fd = value->device_fd;
    unsigned int first_sector = get_first_sector(value, current_cluster);
    struct dir_entry *buf = calloc(1, cluster_size);
    pread(fd, buf, cluster_size, first_sector * sector_size);
    struct file *first_dir_value = NULL;
    struct file *prev_dir_value = NULL;
    struct file *current_dir_value = NULL;

    char *order_bitmap = calloc(1, 32);
    char *order[32];
    unsigned int long_name_counter = 0;
    char end_dir_reached = 0;
    int j = 0;
    while (!end_dir_reached) {
        struct dir_entry *entry = &buf[j++];
        if ((cluster_size / sizeof(struct dir_entry)) <= (j)) {
            // cluster limit reached
            unsigned int fat_record = get_fat_table_value(current_cluster, value->fat_boot->BPB_RsvdSecCnt,
                                                          sector_size, value->device_fd);
            if (fat_record >= 0x0FFFFFF7) {
                // chain end reached or bad cluster...
                end_dir_reached = 1;
            } else {
                current_cluster = fat_record;
                free(buf);
                unsigned int current_sector = get_first_sector(value, current_cluster);
                buf = calloc(1, cluster_size);
                j = 0;
                pread(fd, buf, cluster_size, current_sector * sector_size);
                continue;
            }
        }

        if (entry->filename[0] == 0) {
            // end
            end_dir_reached = 1;
        } else if (entry->filename[0] == 0xE5) {
            // info deleted, but not file
            continue;
        } else if (entry->DIR_Attr == 0x0F) {
            //long file
            struct long_filename *filename = (struct long_filename *) entry;
            // maximum order value == 0x1F
            int current_order = filename->LDIR_Ord & 0x001F;
            order[current_order] = calloc(1, 13);
            order_bitmap[current_order] = 1;
            char *current_buf = order[current_order];
            int buf_offset = 0;
            for (int i = 0; i < 10; i += 2) {
                current_buf[buf_offset++] = (char) filename->LDIR_Name1[i];
            }
            for (int i = 0; i < 12; i += 2) {
                current_buf[buf_offset++] = (char) filename->LDIR_Name2[i];
            }
            for (int i = 0; i < 4; i += 2) {
                current_buf[buf_offset++] = (char) filename->LDIR_Name3[i];
            }
            long_name_counter++;
        } else if ((entry->DIR_Attr & 0x10) == 0x10 || (entry->DIR_Attr & 0x20) == 0x20) {
            //dir or archive
            if (!long_name_counter) {
                char tmp_name[9];
                char tmp_ext[4];
                strncpy(tmp_name, (char *) entry->filename, 8);
                strncpy(tmp_ext, (char *) entry->extension, 3);
                for (int i = 7; i >= 0; --i) {
                    if (tmp_name[i] == 32) {
                        tmp_name[i] = 0;
                    } else {
                        break;
                    }
                }
                for (int i = 2; i >= 0; --i) {
                    if (tmp_ext[i] == 32) {
                        tmp_ext[i] = 0;
                    } else {
                        break;
                    }
                }

                char *filename = calloc(1, 11);
                strcpy(filename, tmp_name);
                if (strlen(tmp_ext)) {
                    strcat(filename, ".");
                    strcat(filename, tmp_ext);
                }

                current_dir_value = init_file(entry, (unsigned char *) filename);

                if (first_dir_value == NULL)
                    first_dir_value = current_dir_value;
                if (prev_dir_value != NULL)
                    prev_dir_value->next = current_dir_value;
                prev_dir_value = current_dir_value;
            } else {
                unsigned char *tmp_str = calloc(1, long_name_counter * 13);
                for (int i = 0; i < 32; ++i) {
                    if (order_bitmap[i] == 1) {
                        strcat((char *) tmp_str, order[i]);
                        order_bitmap[i] = 0;
                        free(order[i]);
                    }
                }

                long_name_counter = 0;
                current_dir_value = init_file(entry, tmp_str);

                if (first_dir_value == NULL)
                    first_dir_value = current_dir_value;
                if (prev_dir_value != NULL)
                    prev_dir_value->next = current_dir_value;
                prev_dir_value = current_dir_value;
            }
        }
    }

    free(order_bitmap);
    free(buf);
    return first_dir_value;
}

int change_dir(struct partition_value *value, char *dir_name) {
    struct file *file = read_dir(value, value->active_cluster);
    while (file != NULL) {
        if (file->type == 'd' && strcmp(dir_name, (char *) file->filename) == 0) {
            value->active_cluster = file->first_cluster;
            free_file(file);
            return 0;
        }
        file = file->next;
    }
    free_file(file);
    return -1;
}

struct partition_value *open_partition(const char *partition) {
    int fd = open(partition, O_RDONLY, 00666);
    struct fat_boot_sector *fat_boot;

    if (fd == -1) {

        return NULL;
    }

    fat_boot = malloc(sizeof(struct fat_boot_sector));
    pread(fd, fat_boot, sizeof(struct fat_boot_sector), 0);

    if (fat_boot->BPB_SecPerClus <= 0) {
        return NULL;
    }

    unsigned int total_sectors = fat_boot->BPB_TotSec32;
    unsigned int fat_size = fat_boot->BPB_FATSz32;
    unsigned int first_data_sector =
            fat_boot->BPB_RsvdSecCnt + (fat_boot->BPB_NumFATs * fat_size);
    unsigned int data_sectors = total_sectors - (fat_boot->BPB_RsvdSecCnt + (fat_boot->BPB_NumFATs * fat_size));
    unsigned int total_clusters = data_sectors / fat_boot->BPB_SecPerClus;

    struct fs_info *fs = malloc(sizeof(struct fs_info));
    pread(fd, fs, sizeof(struct fs_info), fat_boot->BPB_FSInfo * fat_boot->BPB_BytsPerSec);


    if (

            total_clusters < 65525 ||
            /*
             * (2^28-1) = 268435445
             * FAT32 only uses 28 bits not 32
             * Four bits are "reserved for future use"
             */
            total_clusters >= 268435445 ||
            fs->FSI_LeadSig != 0x41615252 ||
            fs->FSI_StrucSig != 0x61417272 ||
            fs->FSI_TrailSig != 0xAA550000
            ) {
        // filesystem not supported
        return NULL;
    }

    // filesystem supported

    unsigned int cluster_size = fat_boot->BPB_BytsPerSec * fat_boot->BPB_SecPerClus;
    //printf("cluster size = %d\n", cluster_size);
    //file size 1,5 cluster

    struct partition_value *part = malloc(sizeof(struct partition_value));
    part->cluster_size = cluster_size;
    part->device_fd = fd;
    part->fat_boot = fat_boot;
    part->fs_info = fs;
    part->first_data_sector = first_data_sector;
    part->active_cluster = fat_boot->BPB_RootClus;

    return part;
}

void close_partition(struct partition_value *part) {
    free(part->fat_boot);
    free(part->fs_info);
    close(part->device_fd);
    free(part);
}