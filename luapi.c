#include "luapi.h"

#include <stdio.h>
#include "second_mode.c"

struct partition_value * partitions[100];

int luapi_ls (lua_State *L)
{
    int client_id = luaL_checknumber(L, 1);
    if (partitions[client_id] == NULL) {
        lua_pushstring(L, "Open partition first!\n");
        return 1;
    }
    char * string = execute_ls(partitions[client_id]);
    printf("%s\n", string);
    lua_pushstring(L, string);
    return 1;
}

int luapi_cp (lua_State *L)
{
    int client_id = luaL_checknumber(L, 1);
    char *from = luaL_checkstring(L, 2);
    char *to = luaL_checkstring(L, 3);
    if (partitions[client_id] == NULL) {
        lua_pushstring(L, "Open partition first!\n");
        return 1;
    }
    execute_cp(partitions[client_id], from, to);
    return 0;
}

int luapi_cd (lua_State *L)
{
    int client_id = luaL_checknumber(L, 1);
    char *path = luaL_checkstring(L, 2);
    if (partitions[client_id] == NULL) {
        lua_pushstring(L, "Open partition first!\n");
        return 1;
    }
    printf("Hi, we are in CD");
    execute_cd(partitions[client_id], path, client_id);
    return 0;
}

int luapi_pwd (lua_State *L)
{
    int client_id = luaL_checknumber(L, 1);
    if (partitions[client_id] == NULL) {
        lua_pushstring(L, "Open partition first!\n");
        return 1;
    }
    lua_pushstring(L, execute_pwd(client_id));
    return 1;
}

int luapi_open(lua_State *L) {
    int client_id = luaL_checknumber(L, 1);
    char *partition_name = luaL_checkstring(L, 2);
    partitions[client_id] = open_fs(partition_name, client_id);
    if (partitions[client_id] == NULL) {
        lua_pushstring(L, "Wrong filesystem type (not FAT32)!\n");
        return 1;
    }
    lua_pushstring(L, "Partition was successfully opened\n");
    return 1;
}

int luaopen_luapi (lua_State *L)
{
    lua_newtable(L);
    lua_pushcfunction (L, luapi_ls);
    lua_setfield (L, -2, "ls");

    lua_pushcfunction (L, luapi_cp);
    lua_setfield (L, -2, "cp");

    lua_pushcfunction (L, luapi_cd);
    lua_setfield (L, -2, "cd");

    lua_pushcfunction (L, luapi_pwd);
    lua_setfield (L, -2, "pwd");

    lua_pushcfunction (L, luapi_open);
    lua_setfield (L, -2, "open");
    return 1;
}