luapi = require('luapi')
ltn12 = require "ltn12"
mime = require "mime"

local client_id = -1
function onConnection(client)
  local _client_id = client_id
  client_id = client_id + 1
  function client:exec(command, arg1, arg2)
    if command == "pwd" then
        NOTE(_client_id)
        return luapi.pwd(_client_id)
    elseif command == "ls" then
        return luapi.ls(_client_id)
    elseif command == "cd" then
        return luapi.cd(_client_id, arg1)
    elseif command == "cp" then
        return luapi.cp(_client_id, arg1, arg2)
    elseif command == "open" then

        if arg2 then
            out_file = "client" .. _client_id .. ".img"

            ltn12.pump.all(
              ltn12.source.string(arg2),
              ltn12.sink.chain(
                mime.decode("base64"),
                ltn12.sink.file(io.open(out_file,"w"))
              )
            )
            local full_path =  "/home/anastasiia/MonaServer/MonaServer" .. out_file
            return luapi.open(_client_id, full_path)
        else
            return luapi.open(_client_id, arg1)
        end

    else
        return "No such command"
    end
  end
end

function onDisconnection(client)
     client_id = client_id - 1
end