#ifndef LAB2_LIBRARY_H
#define LAB2_LIBRARY_H

#include <lua.h>
#include <lauxlib.h>

int luapi_ls(lua_State *L);
int luaopen_luapi (lua_State *L);

int luapi_cp(lua_State *L);

#endif //LAB2_LIBRARY_H
