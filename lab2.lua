local pi = require('luapi')

function split (inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={}
    local i=0
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

function main()
    pi.open('sda3')

    while true do
        io.write('>> ')
        local line = io.read()
        if line == nil then break end

        local parts = split(line, "%s")

        if parts[0] == 'exit' then
            break;
        elseif parts[0] == 'ls' then
            pi.ls()
        elseif parts[0] == 'pwd' then
            pi.pwd()
        elseif parts[0] == 'cd' then
            pi.cd(parts[1])
        elseif parts[0] == 'cp' then
            pi.cp(parts[1], parts[2])
        else
            print('No such command')
        end
    end
end

main();